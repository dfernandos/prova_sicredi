# prova_sicredi

Projeto criado para processo seletivo Stefanini.

# Descriçao do projeto:

Automatizar o preenchimento do formulário e após o deletar.

# Tecnologias

* Java 8
* Maven
* Selenium 3.141.59
* Junit 4.13

# Executar testes
```bash
`mvn test`
```






